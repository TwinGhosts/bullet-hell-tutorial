﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    public float health = 100f;
    public float speed = 3f;

    protected float maxHealth;
    protected Vector2 velocity;

    protected virtual void Awake ()
    {
        // Always set the maximum health to the health set in the inspector
        maxHealth = health;
    }

    protected virtual void Update ()
    {
        // Finalize position by adding the velocity to the position
        transform.position += (Vector3)velocity;
    }
}
