# Bullet Hell Tutorial

A tutorial on how to create a basic 2D bullet hell shooter in Unity.
This tutorial will gloss over concepts like Object Oriented Programming and most of the basics of game development.

It will consist of multiple parts explaining various concepts and implementations for:
*  **Player**               *(Basic Movement)*
*  **Game Manager**         *(Score, Lives, Bombs)*
*  **Utility**              *(Static Util & Stage Bounds)*
*  **Projectiles**          *(Configurability, ECS, Job System?)*
*  **Player**               *(3 Different Shot Types)*
*  **Enemy Entities**       *(Stats & Inheritance)*
*  **Enemy Entities**       *(Shooting)*
*  **Enemy Entities**       *(Basic Pattern)*
*  **Enemy Entities**       *(Movement)*
*  **User Interface**       *(Showing Lives, Bombs and Score)*
*  **Player**               *(Bomb)*
*  **Player**               *(Die + Bomb & Respawn)*
*  **Player**               *(Grazing)*
*  **Spawning Waves**       *(Timelines)*
*  **Bosses**               *(Boss Initialization)*
*  **Bosses**               *(Health & Phases)*
*  **Bosses**               *(Patterns)*
*  **Bosses**               *(Timed Patterns / Surviving)*
*  **Bosses**               *(Death & Mini Boss logic)*
*  **Game Flow**            *(Level Transitions & Data Persistence)*


Dialogue & Textboxes won't be covered. As they deserve a dedicated tutorial of their own. 
Sounds and animations will be covered briefly, but not indepth as it is not the focus of this tutorial.