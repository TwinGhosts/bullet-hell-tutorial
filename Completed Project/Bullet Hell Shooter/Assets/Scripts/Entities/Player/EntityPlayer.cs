﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityPlayer : Entity
{
    protected override void Awake ()
    {
        Util.Player = this;
        base.Awake();
    }

    // Update is called once per frame
    protected override void Update ()
    {
        // Reset to zero at the start to not have friction
        velocity = Vector2.zero;

        // Player input movement
        Movement();

        // Entity class update code
        base.Update();

        // Clamp position to the stage bounds
        var gameManager = Util.GameManager;
        var clampedX = Mathf.Clamp(transform.position.x, gameManager.leftEdge, gameManager.rightEdge);
        var clampedY = Mathf.Clamp(transform.position.y, gameManager.botEdge, gameManager.topEdge);
        var clampedPosition = new Vector2(clampedX, clampedY);
        transform.position = clampedPosition;
    }

    private void Movement ()
    {
        // Get the player movement input
        var input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        // Add to the velocity based on input
        var movementVector = input.normalized * speed * Time.deltaTime;
        velocity += movementVector;
    }
}
