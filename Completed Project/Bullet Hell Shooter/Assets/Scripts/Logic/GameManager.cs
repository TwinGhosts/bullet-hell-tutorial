﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("Player Data")]
    public int playerLives = 3;
    public int playerBombs = 2;
    public int playerScore = 0;

    [Header("Stage Data")]
    public float leftEdge = -5f;
    public float rightEdge = 5f;
    public float topEdge = 10f;
    public float botEdge = -10f;

    private void Awake ()
    {
        Util.GameManager = this;
    }

    private void OnDrawGizmos ()
    {
        Gizmos.color = Color.red;

        // Define all of the bound intersection points
        var topLeft = new Vector3(leftEdge, topEdge);
        var topRight = new Vector3(rightEdge, topEdge);
        var botRight = new Vector3(rightEdge, botEdge);
        var botLeft = new Vector3(leftEdge, botEdge);

        // Draw the stage bounds
        Gizmos.DrawLine(topLeft, topRight);
        Gizmos.DrawLine(botLeft, botRight);
        Gizmos.DrawLine(topLeft, botLeft);
        Gizmos.DrawLine(topRight, botRight);
    }
}
